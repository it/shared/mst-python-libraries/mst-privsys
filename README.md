# mst-authsrv

[[_TOC_]]

## Installation

Command Line:

```bash
pip install mst-privsys --index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
```

requirements.txt

```text
--extra-index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
mst-privsys
```

## Usage

mst-privsys provides an interface to privsys for privilege lookup.

```python
from mst.privsys import check_priv

if check_priv(user, "itapps:cherwell:access"):
    pass
```
